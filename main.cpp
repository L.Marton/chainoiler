#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "ESPAsyncWebServer.h"
#include <EEPROM.h>

#define OIL_PIN 4
const char WebPage[] = "<!DOCTYPE html> <html lang=\"en\"> <head> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> <title>CHAIN OILER</title> <link href=\"data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAADRVQAAAAAALU/kgDPZ7AACltuAN6VyADrltEAiiRrAPW14gCFKmoAbhlUAEADNQBXDUEAOb7bABqCmQAAAAAAEREdERHRERERER4REeERERERFBERQREREREQEREBEREREbu7u7sRERERvMzMyxERERG6qqzLEREREbmZqssRERERt3eayxERERG3d5rLEREREbJ3mssRERERsyeayxERERG1MnrLEREREbhjJ8sRERERG4YysRERERERu7sRERH73wAA+98AAPvfAAD73wAA8A8AAPAPAADwDwAA8A8AAPAPAADwDwAA8A8AAPAPAADwDwAA8A8AAPgfAAD8PwAA\" rel=\"icon\" type=\"image/x-icon\" /> <style> html, body { display: inline-block; text-align: center; height: 100%; border: 0px; padding: 0%; margin: 0%; background-color: rgb(132, 116, 223); }#submit_button { background-color: #4CAF50; color: rgb(8, 23, 40); font-size: 1.2rem; } .slider { -webkit-appearance: none; width: 100%; height: 25px; background: #d3d3d3; outline: none; opacity: 0.7; -webkit-transition: .2s; transition: opacity .2s; } .slider:hover { opacity: 1; } .slider::-webkit-slider-thumb { -webkit-appearance: none; appearance: none; width: 25px; height: 25px; background: #4CAF50; cursor: pointer; } .slider::-moz-range-thumb { width: 25px; height: 25px; background: #4CAF50; cursor: pointer; } #textInterval1 { display: inline-block; } #textInterval2 { display: inline-block; } #textPeriod1 { display: inline-block; } #textPeriod2 { display: inline-block; } </style> </head> <body> <div id=\"main_wrapper\"> <div id=\"main_header\"> <h1>## CHAIN OILER ##</h1> </div> <div id=\"textPeriod1\"> <h3>Oiling period [seconds]: </h3> </div> <div id=\"textPeriod2\"> <h2  id=\"periodText\"></h2> </div> <div class=\"slidecontainer\"> <input type=\"range\" min=\"30\" max=\"90\" value=\"60\" class=\"slider\" id=\"sliderPeriod\" width=\"20\"> </div><div id=\"textInterval1\"> <h3>Oiling interval [miliseconds]: </h3> </div> <div id=\"textInterval2\"> <h2 id=\"intervalText\"></h2> </div> <div class=\"slidecontainer\"> <input type=\"range\" min=\"100\" max=\"2000\" value=\"1050\" class=\"slider\" id=\"sliderInterval\" width=\"20\"> </div> <br> <input id=\"submit_button\" type=\"button\" value=\"Submit\"> </div> <script> \'use strict\';var sliderPeriodVal = document.getElementById(\"sliderPeriod\"); var sliderPeriodValText = document.getElementById(\"periodText\"); sliderPeriodValText.innerHTML = sliderPeriodVal.value; sliderPeriodVal.oninput = function() { sliderPeriodValText.innerHTML = this.value; }; var sliderIntervalVal = document.getElementById(\"sliderInterval\"); var sliderIntervalText = document.getElementById(\"intervalText\"); sliderIntervalText.innerHTML = sliderIntervalVal.value; sliderIntervalVal.oninput = function() { sliderIntervalText.innerHTML = this.value; }; function POST(payload) { let xmlHttp = new XMLHttpRequest(); xmlHttp.onreadystatechange = () => { if (xmlHttp.readyState == 4 && xmlHttp.status == 200) console.log(\"Post done\"); }; xmlHttp.open(\"POST\", window.location.origin, true); xmlHttp.send(payload) }; function submitValues() { console.log(\"Button clicked\"); var periodTime = document.getElementById(\"sliderPeriod\").value; var intervalTime = document.getElementById(\"sliderInterval\").value; var payload = periodTime.toString(10) + intervalTime.toString(10); console.log(payload); POST(payload); }; window.addEventListener(\'load\', function() { console.log(\"Page loaded\"); document.getElementById(\"submit_button\").addEventListener(\"click\", submitValues); }); </script> </body> </html>";
AsyncWebServer server(80);

// Server settings
const char* ssid = "oil";
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

// Times
int periodTime;
int intervalTime;

struct timesStruct
{
  int timePeriod;
  int timeInterval;
  int writtenOnce;
};

void loadTimes()
{
  timesStruct tempMem;
  EEPROM.get(0, tempMem);
  
  Serial.println("Load data....");
  Serial.println(tempMem.timePeriod);
  Serial.println(tempMem.timeInterval);
  Serial.println(tempMem.writtenOnce);


  if(tempMem.writtenOnce != 6666) {
    Serial.println("Data isnt saved");
    periodTime = 30;
    intervalTime = 100;
    return;
  }

  periodTime = tempMem.timePeriod;
  intervalTime = tempMem.timeInterval;

  Serial.print("Loaded Period time: ");
  Serial.println(periodTime, DEC);
  Serial.print("Loaded Interval time: ");
  Serial.println(intervalTime, DEC);
}

void saveTimes()
{
  timesStruct tempMem;
  tempMem.timeInterval = intervalTime;
  tempMem.timePeriod = periodTime;
  tempMem.writtenOnce = 6666;
  EEPROM.put(0, tempMem);
  EEPROM.commit();
  Serial.println("Saved data");
}

void setupServer() {
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", WebPage);
  });

  server.onRequestBody([](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total){
    char tempPeriod[3];
    char tempInterval[5];
    if(len == 5) {
      tempPeriod[0] = data[0];
      tempPeriod[1] = data[1];
      tempPeriod[2] = '\0';
      tempInterval[0] = data[2];
      tempInterval[1] = data[3];
      tempInterval[2] = data[4];
      tempInterval[3] = '\0';
      tempInterval[4] = '\0';
    } else if(len == 6) {
      tempPeriod[0] = data[0];
      tempPeriod[1] = data[1];
      tempPeriod[2] = '\0';
      tempInterval[0] = data[2];
      tempInterval[1] = data[3];
      tempInterval[2] = data[4];
      tempInterval[3] = data[5];;
      tempInterval[4] = '\0';
    } else {
      return;
    }
    
    periodTime = atoi(tempPeriod);
    intervalTime = atoi(tempInterval);

    Serial.print("New Period time: ");
    Serial.println(periodTime, DEC);
    Serial.print("New Interval time: ");
    Serial.println(intervalTime, DEC);

    saveTimes();
  });

    // In any other case fall back to 404
  server.onNotFound([](AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "404 - Not Found");
  });

  // Listen and serve
  server.begin();
  Serial.println("Oiler server listening on PORT 80");
  Serial.println();

}

void SetupAP() {
  WiFi.persistent(false);
  WiFi.softAP(ssid, NULL);
  delay(100);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  Serial.println("Wifi AP has been set up");
  Serial.println("SSID: oil");
  Serial.println("IP  : 192.168.1.1\n");
}

void setup() {
  EEPROM.begin(20);
  Serial.begin(115200);
  loadTimes();
  pinMode(OIL_PIN, OUTPUT);
  Serial.println("Setup server...");
  SetupAP();
  setupServer();

  timesStruct testOut;
  EEPROM.get(0, testOut);
}

void loop() {
  digitalWrite(OIL_PIN, HIGH);
  delay(intervalTime);
  digitalWrite(OIL_PIN, LOW);
  delay(periodTime * 1000);
}